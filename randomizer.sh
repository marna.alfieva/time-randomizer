#!/bin/bash
#Time randomizer
morning_h=`shuf -i 08-10 -n1`
morning_min=`shuf -i 00-59 -n1`
evening_h=`shuf -i 16-19 -n1`
evening_min=`shuf -i 00-59 -n1`

echo "Time in the morning is: $morning_h:$morning_min"
echo "Time in the evening is: $evening_h:$evening_min"

